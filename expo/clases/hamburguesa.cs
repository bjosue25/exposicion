﻿using System;
using System.Collections.Generic;
using System.Text;

namespace expo.clases
{

    /// la clase abstracta del Prototype 
    /// </resumen>
    abstract class HamburguesaPrototype
    {
        public abstract HamburguesaPrototype Clone();
    }
    //clase Hamburguesa hereda HamburguesaPrototype
    class Hamburguesa : HamburguesaPrototype
    {
        private string Pan;
        private string Carne;
        private string Queso; 
        private string Papas;
        

        public Hamburguesa(string pan, string carne, string queso, string papas)
        {
            Pan = pan;
            Carne = carne;
            Queso = queso;
            Papas = papas;
            
        }
        //public override se usa para indicar que el método que se está definiendo modificará el comportamiento del definido en la clase base.
        public override HamburguesaPrototype Clone()
        {
            string ingredientList = GetIngredientList();
            Console.WriteLine("Clonacion de los ingredientes de las hamburguesa: {0}", ingredientList.Remove(ingredientList.LastIndexOf(",")));

            return MemberwiseClone() as HamburguesaPrototype;
        }

        private string GetIngredientList()
        {
            var ingredientList = "";
            if (!string.IsNullOrWhiteSpace(Pan))
            {
                ingredientList += Pan + ", ";
            }
            if (!string.IsNullOrWhiteSpace(Carne))
            {
                ingredientList += Carne + ", ";
            }
            if (!string.IsNullOrWhiteSpace(Queso))
            {
                ingredientList += Queso + ", ";
            }
            if (!string.IsNullOrWhiteSpace(Papas))
            {
                ingredientList += Papas + ", ";
            }
           
            return ingredientList;
        }
    }

     class HamburguesaMenu
    {
        private Dictionary<string, HamburguesaPrototype> _Hamburguesas = new Dictionary<string, HamburguesaPrototype>();

        public HamburguesaPrototype this[string name]
        {
            get { return _Hamburguesas[name]; }
            set { _Hamburguesas.Add(name, value); }
        }
    }
}
