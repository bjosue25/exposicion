﻿using expo.clases;
using System;

namespace expo
{
    class Program
    {
        static void Main(string[] args)
        {
            HamburguesaMenu HamburguesaMenu = new HamburguesaMenu();

            // Iniciar como predeterminado con Hamburguesa
            HamburguesaMenu["hamburguesanormal"] = new Hamburguesa("Pan", "Carne","", "Queso, Papas");
            HamburguesaMenu["hamburguesaespecialJ"] = new Hamburguesa("Pan", "Huevo", "Jamon", "Carne , Papas");
            HamburguesaMenu["hamburguesadoble"] = new Hamburguesa("Carne", "Pan", "Carne", "Huevo, Jamon, Papas");

            // Se agrega las Hamburguesa personalizada
            HamburguesaMenu["hambuguesaBBQ"] = new Hamburguesa("Queso", "Pan, SalsadeBBQ", "Carne", "lechuga, Tomate, Huevo");
            HamburguesaMenu["hamburguesatriple"] = new Hamburguesa("Carne", "Pan, Jamon, Carne", "Carne", "Tomate, Lechuga");
            HamburguesaMenu["hamburguesadepollo"] = new Hamburguesa("Pan", "Huevo", "Pollo", "Cebolla, Tomate");

            
            //Ahora podemos clonar la  Hamburguesa.
            Hamburguesa Hamburguesa1 = HamburguesaMenu["hamburguesanormal"].Clone() as Hamburguesa;
            Hamburguesa Hamburguesa2 = HamburguesaMenu["hamburguesadoble"].Clone() as Hamburguesa;
            Hamburguesa Hamburguesa3 = HamburguesaMenu["hamburguesatriple"].Clone() as Hamburguesa;

            Console.ReadKey();
        }
    }
}
